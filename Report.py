from os import waitpid
import xlsxwriter
from xlsxwriter.utility import xl_rowcol_to_cell
from pickle import load, loads
import argparse

def get_report(excel, month, year, daily):

    archivo_db = excel[:-5]+".db"

    workbook = xlsxwriter.Workbook(excel) 
    worksheet = workbook.add_worksheet() 
    row = 0
    column = 0

    with open(archivo_db,'rb') as f:
        Hours = load(f)

    Hours = loads(Hours)
    monthStr = str(month)

    for (key,value) in Hours.items():
        if daily == True: 
            if key == "day":
                for i in range(32):
                    if i == 0: continue
                    worksheet.write(i,column,str(i) + "/" + str(month) + "/" + str(year))
                worksheet.write(33,column, "Total")
                column += 1
                continue
            
            worksheet.write(0,column,key)
            hours = dict(value.items())
            hours_sum = 0
            for i in range(32):
                if i in hours:
                    hours_sum += hours[i][0].seconds //3600
                    worksheet.write(row+i, column, hours[i][0].seconds //3600)
                    worksheet.write(row+i, column+1, hours[i][1])

            start_cell = xl_rowcol_to_cell(1, column)  
            end_cell = xl_rowcol_to_cell(31, column)
            worksheet.write_formula(33, column, f'=SUM({start_cell}:{end_cell})')

            column += 2
        else:
            if key == "day":
                continue
            total = 0
            worksheet.write(row, column, "Nombre")
            worksheet.write(row+1, column, "Horas trabajadas del 1/" +monthStr +"/" + str(year) + " al 7/"+monthStr +"/" + str(year))
            worksheet.write(row+2, column, "Horas trabajadas del 8/" + monthStr +"/" + str(year) + " al 14/"+monthStr +"/" + str(year))
            worksheet.write(row+3, column, "Horas trabajadas del 15/" + monthStr + "/" + str(year) + " al 21/"+monthStr +"/" + str(year))
            worksheet.write(row+4, column, "Horas trabajadas del 22/" + monthStr +"/" + str(year) + " al 28/"+monthStr +"/" + str(year))
            worksheet.write(row+5, column, "Horas trabajadas del 29/" + monthStr + "/" + str(year) + " hasta fin de mes")
            worksheet.write(row+6, column, "Horas totales del mes: ")
            worksheet.write(row,column+1,key)
            week_hours = [0,0,0,0,0]
            for (w,h) in value.items():
                week = int(int(w) // 7.1)
                week_hours[week] += h[0].total_seconds() // 3600
                
            for index,w in enumerate(week_hours):
                worksheet.write(row + int(index + 1),column+1,w)
    
            worksheet.write_formula(row + 6, column+1, f'=SUM(B{row+2}:B{row+6})')
            row+=7


    workbook.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--mes',
                        required=True)
    parser.add_argument('--year',
                        required=True)   
    parser.add_argument('--excel',
                        default='Report.xlsx',
                        required=False)
    parser.add_argument('--kind',
                        required=True)
    args = parser.parse_args()
    excel = args.excel
    daily = args.kind == "daily"
    mes = args.mes
    year = args.year
    get_report(excel, mes, year, daily)

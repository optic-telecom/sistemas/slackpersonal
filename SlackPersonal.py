import os
import slack_sdk
import time
import calendar
import argparse
import xlsxwriter
import traceback
from slack_sdk import WebClient
from pickle import dump, dumps, load, loads
from datetime import datetime, timedelta, timezone
from collections import defaultdict
from . import settings

from .Report import get_report

zero = timedelta(seconds=0)


def mainRequest(tk, channel, excel, month, year, canal, kind):


    Prevdata = {}
    previousData = {}
    #Set month and year
    archivo_db = excel[:-5]+".db"
    daily = kind == "daily"
    Hours = defaultdict(dict) if daily == True else {}

    _,days = calendar.monthrange(year, month)
    os.chdir(settings.REPORT_DIR)

    client = slack_sdk.WebClient(token=tk)
    

    if os.path.exists(archivo_db):
        with open(archivo_db,'rb') as rfp: 
            previousData = load(rfp)
        Prevdata = loads(previousData)

    for i in range(1,days+1):
        Hours["day"] = i

        if i<=Prevdata.get("day", 0):
            continue
        
        startDate=datetime(year,month,i,4,0,0)
        oldest =time.mktime(startDate.timetuple())
        endDate = startDate+ timedelta(hours=23, minutes=59)
        latest=time.mktime(endDate.timetuple())
        
        try:
            response = client.conversations_history(
                        limit=999,
                        channel=channel,
                        token=settings.MSG_REQUEST_TOKEN,
                        oldest=oldest,
                        latest=latest
            )

            data = response.data.get("messages")
            if len(data) > 0:
                for message in data:
                    data_user = client.users_info(token=tk, user=message["user"])["user"]
                    try:
                        userName = data_user["real_name"]
                    except :
                        userName = data_user["profile"]["real_name"]
                        
                    user = message["user"]
                    msg = message["text"].upper()
                    msg_split = msg.split(' ')
                    if 'IN' in msg_split:
                        inHour =  datetime.fromtimestamp(float(message["ts"]), tz=timezone.utc)
                        outMsg = next((msg for msg in data if ('OUT' in msg["text"].upper() and msg["user"] == user)), None)

                        if outMsg != None: 
                            outHour =  datetime.fromtimestamp(float(outMsg["ts"]), tz=timezone.utc)
                            WorkedHours = outHour - inHour

                            if userName not in Hours:
                                dict_hours = {userName: {}}
                                Hours.update(dict_hours)
                            in_time = inHour - timedelta(hours=4)
                            out_time = outHour - timedelta(hours=4)
                            Hours[userName][i] = [WorkedHours, str("IN " + in_time.strftime("%H:%M") + " - OUT " + out_time.strftime("%H:%M"))]

                        else:
                            pass    
                    else:
                        pass

        except Exception:
            client.chat_postMessage(channel=canal, text=str("error " + traceback.format_exc()))


        pickled_obj = dumps(Hours)
        with open(archivo_db, "wb") as f:
            dump(pickled_obj, f)

    get_report(excel, month, year, daily)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--excel',
                        default='Report.xlsx',
                        required=False)
    parser.add_argument('--token',
                        required=True)
    parser.add_argument('--mes',
                        required=True)
    parser.add_argument('--year',
                        required=True)
    parser.add_argument('--channel',
                        required=True)
    parser.add_argument('--kind',
                        required=True)
    args = parser.parse_args()
    excel = args.excel
    kind = args.kind
    month = int(args.mes)
    tk = args.token
    channel = args.channel
    year = int(args.year)
    mainRequest(tk, channel, excel, month, year, channel, kind)